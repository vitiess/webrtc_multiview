//------------------------------------------------------------
// Globals
//------------------------------------------------------------
const IDLE_MODE = "Idle"
const CAMERA_MODE = "Camera";
const VIEWER_MODE = "Viewer";
const ENTERING_CAMERA_MODE = "EnteringCamera";
const ENTERING_VIEWER_MODE = "EnteringViewer";
const DEBUG = "debug";
const INFO = "info";
const UNKNOWN_USER = "Unknown";
let signalServiceClient = null;
let video = null;
let webRtcPeer = null;
let applicationMode = IDLE_MODE;
let nickname = UNKNOWN_USER;
const logDebug = true;
const logInfo = true;

//------------------------------------------------------------
// Socket connection business logic
//------------------------------------------------------------

function createSignalServiceClient() {
   const socket = new SockJS("/message");
   const stompClient = Stomp.over(socket);
   stompClient.connect(
       {}, // Headers
       function() { // Success callback
          onSocketConnected();
       },
       function(error) { // Error callback
          onSocketConnectionError(error);
       }
   );
   return stompClient;
}

function onSocketConnected() {
   signalServiceClient.subscribe("/user/connect/reply", function(reply) {
      onConnectReply(JSON.parse(reply.body))
   });
   signalServiceClient.subscribe("/user/errors", function(error) {
      onSignalingServiceRequestError(JSON.parse(error.body));
   });
   signalServiceClient.subscribe("/user/addIceCandidate", function(message) {
      onRemoteIceCandidate(JSON.parse(message.body));
   });
   signalServiceClient.subscribe("/participant", function(message) {
      onSessionParticipantsUpdate(JSON.parse(message.body))
   });
}

function onSocketConnectionError(error) {
   onApplicationError(toErrorMessage(error));
   $("#joinAsCallee").prop("disabled", true)
   $("#joinAsCaller").prop("disabled", true)
}

//------------------------------------------------------------
// Application business logic
//------------------------------------------------------------

function joinAsCallee() {
   if (applicationMode !== IDLE_MODE) {
      onApplicationError(toErrorMessage("You already joined as a " + applicationMode === VIEWER_MODE ? "viewer" : "camera"))
      return;
   }
   $("#mainMenu").hide()
   $("#spinner").show()
   applicationMode = ENTERING_CAMERA_MODE
   let options = {
      localVideo: video,
      onicecandidate: onLocalIceCandidate,
      mediaConstraints: { audio: false, video: true }
   };
   webRtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function(error) {
      if (error) {
         onApplicationError(toErrorMessage(error));
      } else {
         log(DEBUG, "Initiating generation of local SDP offer (callee)");
         this.generateOffer(onCalleeSdpOfferGenerated);
      }
   });
}

function onCalleeSdpOfferGenerated(error, sdpOffer) {
   if (error) {
      onApplicationError(toErrorMessage(error));
   } else {
      log(DEBUG, "Successfully generated local SDP offer. Sending it to signaling server.");
      let connectAsCalleeRequest = {
         actor: {
            name: nickname
         },
         sdpOffer: {
            offer: sdpOffer
         },
         role: "PRESENTER"
      }
      signalServiceClient.send("/app/connect", {}, JSON.stringify(connectAsCalleeRequest));
   }
}

function onLocalIceCandidate(candidate) {
   log(INFO, "Received local ICE candidate: " + JSON.stringify(candidate));
   let addIceCandidateMessage = {
      candidate: candidate
   };
   log(DEBUG, "Sending local ICE candidate to signaling service.");
   signalServiceClient.send("/app/addIceCandidate", {}, JSON.stringify(addIceCandidateMessage));
}

function onRemoteIceCandidate(candidate) {
   log(INFO, "Received remote ICE candidate: " + JSON.stringify(candidate));
   webRtcPeer.addIceCandidate(candidate, function(error) {
      if (error) {
         onApplicationError(toErrorMessage(error));
      } else {
         log(DEBUG, "Accepted remote ICE candidate");
      }
   });
}

function joinAsCaller() {
   if (applicationMode !== IDLE_MODE) {
      onApplicationError(toErrorMessage("You already joined as a " + applicationMode === VIEWER_MODE ? "viewer" : "camera"))
      return;
   }
   $("#mainMenu").hide();
   $("#spinner").show();
   applicationMode = ENTERING_VIEWER_MODE;
   let options = {
      remoteVideo: video,
      onicecandidate: onLocalIceCandidate,
      mediaConstraints: { audio: false, video: true }
   };
   webRtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function(error) {
      if (error) {
         onApplicationError(toErrorMessage(error));
      } else {
         log(DEBUG, "Initiating generation of local SDP offer (callee)");
         this.generateOffer(onCallerSdpOfferGenerated);
      }
   });
}

function onCallerSdpOfferGenerated(error, sdpOffer) {
   if (error) {
      onApplicationError(toErrorMessage(error));
   } else {
      log(DEBUG, "Successfully generated local SDP offer. Sending it to signaling server.");
      let connectAsCallerRequest = {
         actor: {
            name: nickname
         },
         sdpOffer: {
            offer: sdpOffer
         },
         role: "VIEWER"
      }
      signalServiceClient.send("/app/connect", {}, JSON.stringify(connectAsCallerRequest));
   }
}

function onConnectReply(reply) {
   log(INFO, "Got SDP offer reply from signaling service: " + JSON.stringify(reply));
   webRtcPeer.processAnswer(reply.sdpAnswer.answer, function(error) {
      if (error) {
         onApplicationError(toErrorMessage(error))
      } else {
         log(INFO, "Accepted signaling service SDP answer.");
         applicationMode = applicationMode === ENTERING_CAMERA_MODE ? CAMERA_MODE : VIEWER_MODE;
         log(INFO, "Changed application mode to " + applicationMode);
         addParticipants(reply.participants);
         showActionUI();
      }
   });
}

function onSignalingServiceRequestError(error) {
   onApplicationError(error);
}

function onApplicationError(error) {
   showError(error.errorMessage);
   if (applicationMode === IDLE_MODE ||
       applicationMode === ENTERING_VIEWER_MODE ||
       applicationMode === ENTERING_CAMERA_MODE)
   {
      resetUI(true);
      if (webRtcPeer !== null) {
         webRtcPeer.dispose();
         webRtcPeer = null;
      }
   }
}

function disconnect() {
   if (applicationMode === CAMERA_MODE || applicationMode === VIEWER_MODE) {
      signalServiceClient.send("/app/disconnect", {}, "");
      resetUI(false);
      webRtcPeer.dispose();
      webRtcPeer = null;
   } else {
      onApplicationError(toErrorMessage("You are not connected."));
   }
}

function onSessionParticipantsUpdate(participantsUpdateMessage) {
   log(INFO, "Received participants update :" + JSON.stringify(participantsUpdateMessage));
   if (participantsUpdateMessage.added) {
      addParticipants(participantsUpdateMessage.added);
   }
   if (participantsUpdateMessage.removed) {
      removeParticipants(participantsUpdateMessage.removed);
   }
}

function addParticipants(participants) {
   log(DEBUG, "Adding " + JSON.stringify(participants) + " to session.");
   const currentParticipants = new Set();
   $("#participantsList > tr").each(function() {
      currentParticipants.add($(this).attr("id"));
   });
   for (let i = 0; i < participants.length; i++) {
      const participantName = participants[i].name;
      const participantRole = participants[i].role.toLowerCase();
      const participantId = participants[i].id;
      if (!currentParticipants.has(participantId)) {
         $("#participantsList").append("<tr id=\"" + participantId + "\">" + "<td>" + participantName + "</td><td>" + participantRole + "</td></tr>");
      }
   }
}

function removeParticipants(participants) {
   log(DEBUG, "Removing " + JSON.stringify(participants) + " to session.");
   participants.forEach(p => $("#" + p.id).remove());
}

function updateGridLayout() {
   hideErrors();
   let gridWidth = getFieldValueOrDefault("width", "width",800);
   let gridHeight = getFieldValueOrDefault("height", "height", 600);
   let horizontalSpacing = getFieldValueOrDefault("horizontalSpacing", "horizontal spacing", 0);
   let verticalSpacing = getFieldValueOrDefault("verticalSpacing", "vertical spacing",  0);
   let topPadding = getFieldValueOrDefault("topPadding", "top padding", 0);
   let rightPadding = getFieldValueOrDefault("rightPadding", "right padding",  0);
   let bottomPadding = getFieldValueOrDefault("bottomPadding", "bottom padding",  0);
   let leftPadding = getFieldValueOrDefault("leftPadding", "left padding", 0);
   let maxRows = getFieldValueOrDefault("maxRows", "max rows",  0);
   let maxCols = getFieldValueOrDefault("maxColumns", "max columns",  0);
   let alignmentPolicy = getFieldValueOrDefault("alignmentPolicy", "alignment policy", 0);
   log(DEBUG, "Updating grid size to " + gridWidth + " X " + gridHeight);
   log(DEBUG, "Updating spacing to [h=" + horizontalSpacing + ", v=" + verticalSpacing + "]");
   log(DEBUG, "Updating padding to [t=" + topPadding + ", r=" + rightPadding + ", b=" + bottomPadding + ", l=" + leftPadding);
   log(DEBUG, "Updating constraints to [maxRows=" + maxRows + ", maxColumns=" + maxCols + ", alignmentPolicy=" + alignmentPolicy + "]");

   let updateLayoutRequest = {
      width: gridWidth,
      height: gridHeight,
      horizontalSpacing: horizontalSpacing,
      verticalSpacing: verticalSpacing,
      topPadding: topPadding,
      rightPadding: rightPadding,
      bottomPadding: bottomPadding,
      leftPadding: leftPadding,
      maxRows: maxRows,
      maxCols: maxCols,
      alignmentPolicy: alignmentPolicy
   };
   signalServiceClient.send("/app/updateLayout", {}, JSON.stringify(updateLayoutRequest));
}

function magnify() {
   let portNumber = getFieldValueOrDefault("magnificationPort", "port number", "-1");
   log(DEBUG, "Magnification of port" + portNumber);
   let magnifyRequest = {
      port: portNumber
   };
   signalServiceClient.send("/app/magnify", {}, JSON.stringify(magnifyRequest));
}

function clearMagnification() {
   signalServiceClient.send("/app/clearMagnification", {}, JSON.stringify({}));
}

function setMetadata() {
   let portNumber = getFieldValueOrDefault("metadataPortId", "metadata port", "-1");
   let friendlyName = getFieldStringValueOrDefault("friendlyName", "friendly name", "Unnamed");
   let setMetadataRequest = {
      port: portNumber,
      friendlyName: friendlyName
   };
   signalServiceClient.send("/app/setPortMetadata", {}, JSON.stringify(setMetadataRequest));
}

function getFieldValueOrDefault(field, fieldName, defaultValue) {
   let value = $("#" + field).val();
   if (value) {
      let parsedValue = parseInt(value)
      if (isNaN(parsedValue) || parsedValue < 0) {
         showError("Field '" + fieldName + "' must be a non-negative integer.");
      } else {
         return parsedValue;
      }
   } else {
      return defaultValue;
   }
}

function getFieldStringValueOrDefault(field, fieldName, defaultValue) {
   let value = $("#" + field).val();
   if (value) {
      return value;
   } else {
      return defaultValue;
   }
}

function resetLayoutOptions() {
   hideErrors();
   $("#width").val("");
   $("#height").val("");
   $("#verticalSpacing").val("");
   $("#horizontalSpacing").val("");
   $("#topPadding").val("");
   $("#rightPadding").val("");
   $("#bottomPadding").val("");
   $("#leftPadding").val("");
   $("#maxRows").val("");
   $("#maxColumns").val("");
   $("#alignmentPolicy").val("0");
}

//------------------------------------------------------------
// UI business logic
//------------------------------------------------------------

function resetUI(keepErrorDisplay) {
   $("#joinAsCallee").prop("disabled", true);
   $("#joinAsCaller").prop("disabled", true);
   $("#videoDisplay").hide();
   $("#mainMenu").show();
   $("#spinner").hide();
   $("#callerView").hide();
   $("#calleeView").hide();
   $("#dashboard").hide();
   $("#layoutControls").hide();
   $("#instructions").show();
   if (keepErrorDisplay === false) {
      hideErrors();
   }
   applicationMode = IDLE_MODE;
   nickname = UNKNOWN_USER;
   $("#nickname").val("");
}

function hideErrors() {
   $("#errorPanel").hide();
}

function showError(error) {
   let errorPanel = $("#errorPanel")
   errorPanel.html("<p>" + error + "</p>");
   errorPanel.show()
}

function showActionUI() {
   hideErrors();
   $("#spinner").hide();
   $("#dashboard").show();
   $("#videoDisplay").show();
   $("#instructions").hide();
   let sessionRole = applicationMode === CAMERA_MODE ? "presenter" : "viewer";
   $("#identificationMessage").html("Hello " + nickname + ", you are connected as a <b>" + sessionRole + "</b>!");
   if (applicationMode === VIEWER_MODE) {
      $("#layoutControls").show();
   }
   video.play();
}

function updateNickname(e) {
   const name = e.target.value;
   if (name) {
      nickname = name;
      $("#joinAsCaller").prop("disabled", false);
      $("#joinAsCallee").prop("disabled", false);
   } else {
      $("#joinAsCaller").prop("disabled", true);
      $("#joinAsCallee").prop("disabled", true);
   }
}

//------------------------------------------------------------
// Misc
//------------------------------------------------------------

function log(level, message) {
   if (level === DEBUG) {
      if (logDebug === true) {
         console.log(message)
      }
   } else if (level === INFO) {
      if (logInfo === true) {
         console.log(message)
      }
   }
}

function toErrorMessage(error) {
   return {
      errorMessage: error
   }
}

//------------------------------------------------------------
// Main
//------------------------------------------------------------

$(function() {
   resetUI(false);
   $("#joinAsCallee").click(function() {
      joinAsCallee();
   });
   $("#joinAsCaller").click(function() {
      joinAsCaller();
   });
   $("#disconnect").click(function() {
      disconnect();
   });
   $("#updateGridLayoutButton").click(function () {
      updateGridLayout();
   });
   $("#resetLayoutOptions").click(function () {
      resetLayoutOptions();
   });
   $("#magnify").click(function () {
      magnify();
   });
   $("#clearMagnification").click(function () {
      clearMagnification();
   });
   $("#setMetadata").click(function() {
      setMetadata();
   });
   $("#nickname").bind('input', updateNickname)
   video = $("#video")[0];
   signalServiceClient = createSignalServiceClient();
});

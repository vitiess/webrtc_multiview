package com.appersonic.multiview.model

data class SdpOffer(val offer: String?) {
    init {
        require(offer != null && offer.isNotBlank()) {
            "Session Description Protocol (SDP) offer must not be null or blank."
        }
    }
}

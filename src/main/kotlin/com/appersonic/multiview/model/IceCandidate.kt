package com.appersonic.multiview.model

data class IceCandidate(
    val candidate: String?,
    val sdpMid: String?,
    val sdpMLineIndex: Int?
)
{
    init {
        require(candidate != null && candidate.isNotBlank()) { "ICE candidate property must not be null or blank." }
        require(sdpMid != null && sdpMid.isNotBlank()) { "ICE sdpMid property must not be null or blank." }
        requireNotNull(sdpMLineIndex) { "ICE sdpMLineIndex must not be null." }
    }
}

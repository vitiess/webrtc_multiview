package com.appersonic.multiview.model

data class SdpAnswer(val answer: String?) {
    init {
        require(answer != null && answer.isNotBlank()) {
            "Session Description Protocol (SDP) answer must not be null or blank."
        }
    }
}

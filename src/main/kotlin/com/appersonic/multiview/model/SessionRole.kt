package com.appersonic.multiview.model

enum class SessionRole {
    PRESENTER,
    VIEWER
}

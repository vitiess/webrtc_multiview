package com.appersonic.multiview.model

class SignalingServerException(
    override val message: String?,
    override val cause: Throwable? = null
): RuntimeException(message, cause)
{
    companion object {
        fun createWithMessage(message: String): SignalingServerException {
            return SignalingServerException(message)
        }
        fun createWithMessageAndCause(message: String, cause: Throwable): SignalingServerException {
            return SignalingServerException(message, cause)
        }
    }
}

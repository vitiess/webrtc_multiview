package com.appersonic.multiview.model

data class SessionId(val id: String?) {
    init {
        require(id != null && id.isNotBlank()) {
            "session identifier must not be null or blank."
        }
    }
}

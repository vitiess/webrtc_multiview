package com.appersonic.multiview.model

data class SessionParticipant(val name: String, val role: SessionRole, val id: String)

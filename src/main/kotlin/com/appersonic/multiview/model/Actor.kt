package com.appersonic.multiview.model

data class Actor(val name: String?) {
    init {
        require(name != null && name.isNotBlank()) {
            "Actor name must not be blank."
        }
    }
}

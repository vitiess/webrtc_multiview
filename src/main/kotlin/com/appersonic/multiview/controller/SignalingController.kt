package com.appersonic.multiview.controller

import com.appersonic.multiview.constants.ADD_ICE_CANDIDATE_DESTINATION_PREFIX
import com.appersonic.multiview.constants.CLEAR_MAGNIFICATION_PREFIX
import com.appersonic.multiview.constants.CONNECT_DESTINATION_PREFIX
import com.appersonic.multiview.constants.DISCONNECT_DESTINATION_PREFIX
import com.appersonic.multiview.constants.ERRORS_DESTINATION_PREFIX
import com.appersonic.multiview.constants.INTERNAL_SERVER_ERROR_MESSAGE
import com.appersonic.multiview.constants.MAGNIFY_PREFIX
import com.appersonic.multiview.constants.MESSAGE_REPLY_SUFFIX
import com.appersonic.multiview.constants.SET_PORT_METADATA_PREFIX
import com.appersonic.multiview.constants.UPDATE_FLEX_GRID_LAYOUT_PREFIX
import com.appersonic.multiview.message.AddIceCandidateMessage
import com.appersonic.multiview.message.ClearMagnificationMessage
import com.appersonic.multiview.message.ConnectMessage
import com.appersonic.multiview.message.ConnectReply
import com.appersonic.multiview.message.DisconnectMessage
import com.appersonic.multiview.message.ErrorReply
import com.appersonic.multiview.message.MagnifyMessage
import com.appersonic.multiview.message.SessionAwareMessage
import com.appersonic.multiview.message.SetPortMetadataMessage
import com.appersonic.multiview.message.SignalingMessageHandler
import com.appersonic.multiview.message.UpdateFlexGridLayoutMessage
import com.appersonic.multiview.model.SessionId
import com.appersonic.multiview.model.SignalingServerException
import org.springframework.messaging.handler.annotation.MessageExceptionHandler
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.simp.SimpMessageHeaderAccessor
import org.springframework.messaging.simp.annotation.SendToUser
import org.springframework.stereotype.Controller

@Controller
class SignalingController(private val messageHandler: SignalingMessageHandler) {
    @MessageMapping(CONNECT_DESTINATION_PREFIX)
    @SendToUser("$CONNECT_DESTINATION_PREFIX$MESSAGE_REPLY_SUFFIX")
    fun connect(
        message: ConnectMessage,
        messageHeaderAccessor: SimpMessageHeaderAccessor
    ): ConnectReply
    {
        return messageHandler.handleConnectMessage(
            SessionAwareMessage(message, readSessionId(messageHeaderAccessor))
        )
    }

    @MessageMapping(DISCONNECT_DESTINATION_PREFIX)
    @SendToUser("$DISCONNECT_DESTINATION_PREFIX$MESSAGE_REPLY_SUFFIX")
    fun disconnect(
        messageHeaderAccessor: SimpMessageHeaderAccessor
    )
    {
        messageHandler.handleDisconnectMessage(
            SessionAwareMessage(DisconnectMessage(), readSessionId(messageHeaderAccessor))
        )
    }

    @MessageMapping(ADD_ICE_CANDIDATE_DESTINATION_PREFIX)
    @SendToUser("$ADD_ICE_CANDIDATE_DESTINATION_PREFIX$MESSAGE_REPLY_SUFFIX")
    fun addIceCandidate(
        message: AddIceCandidateMessage,
        messageHeaderAccessor: SimpMessageHeaderAccessor
    )
    {
        messageHandler.handleIceCandidateMessage(
            SessionAwareMessage(message, readSessionId(messageHeaderAccessor))
        )
    }

    @MessageMapping(UPDATE_FLEX_GRID_LAYOUT_PREFIX)
    @SendToUser("$UPDATE_FLEX_GRID_LAYOUT_PREFIX$MESSAGE_REPLY_SUFFIX")
    fun updateFlexGridLayout(
        message: UpdateFlexGridLayoutMessage,
        messageHeaderAccessor: SimpMessageHeaderAccessor
    )
    {
        messageHandler.handleFlexGridLayoutUpdate(
            SessionAwareMessage(message, readSessionId(messageHeaderAccessor))
        )
    }

    @MessageMapping(MAGNIFY_PREFIX)
    @SendToUser("$MAGNIFY_PREFIX$MESSAGE_REPLY_SUFFIX")
    fun magnify(
        message: MagnifyMessage,
        messageHeaderAccessor: SimpMessageHeaderAccessor
    )
    {
        messageHandler.handleMagnificationMessage(
            SessionAwareMessage(message, readSessionId(messageHeaderAccessor))
        )
    }

    @MessageMapping(CLEAR_MAGNIFICATION_PREFIX)
    @SendToUser("$CLEAR_MAGNIFICATION_PREFIX$MESSAGE_REPLY_SUFFIX")
    fun clearMagnification(
        message: ClearMagnificationMessage,
        messageHeaderAccessor: SimpMessageHeaderAccessor
    )
    {
        messageHandler.handleClearMagnificationMessage(
            SessionAwareMessage(message, readSessionId(messageHeaderAccessor))
        )
    }

    @MessageMapping(SET_PORT_METADATA_PREFIX)
    @SendToUser("$SET_PORT_METADATA_PREFIX$MESSAGE_REPLY_SUFFIX")
    fun setPortMetadata(
        message: SetPortMetadataMessage,
        messageHeaderAccessor: SimpMessageHeaderAccessor
    )
    {
        messageHandler.handleSetPortMetadataMessage(
            SessionAwareMessage(message, readSessionId(messageHeaderAccessor)))
    }

    @MessageExceptionHandler
    @SendToUser(ERRORS_DESTINATION_PREFIX)
    fun handleException(exception: Throwable): ErrorReply {
        return if (exception is SignalingServerException || exception is IllegalArgumentException) {
            ErrorReply(exception.message!!)
        } else {
            ErrorReply(INTERNAL_SERVER_ERROR_MESSAGE)
        }
    }

    private companion object {
        fun readSessionId(messageHeaderAccessor: SimpMessageHeaderAccessor): SessionId {
            return SessionId(messageHeaderAccessor.sessionId)
        }
    }
}

package com.appersonic.multiview.config

import com.appersonic.multiview.message.SignalingMessageHandler
import com.appersonic.multiview.message.SignalingMessageSender
import com.appersonic.multiview.message.SingleSessionSignalingHandler
import com.appersonic.multiview.message.StompSignalingMessageSender
import com.appersonic.multiview.webrtc.WebRtcSessionFactory
import com.appersonic.multiview.webrtc.kurento.KurentoCompositeWebRtcSessionFactory
import org.kurento.client.KurentoClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.messaging.simp.SimpMessagingTemplate

@Configuration
class ApplicationConfig {
    @Bean
    fun kurentoClient(): KurentoClient {
        return KurentoClient.create()
    }

    @Bean
    fun signalingMessageSender(messagingTemplate: SimpMessagingTemplate): SignalingMessageSender {
        return StompSignalingMessageSender(messagingTemplate)
    }

    @Bean
    fun webRtcSessionFactory(
        kurentoClient: KurentoClient,
        signalingMessageSender: SignalingMessageSender
    ): WebRtcSessionFactory
    {
        return KurentoCompositeWebRtcSessionFactory(kurentoClient, signalingMessageSender)
    }

    @Bean
    fun signalingMessageHandler(webRtcSessionFactory: WebRtcSessionFactory): SignalingMessageHandler {
        return SingleSessionSignalingHandler(webRtcSessionFactory)
    }
}

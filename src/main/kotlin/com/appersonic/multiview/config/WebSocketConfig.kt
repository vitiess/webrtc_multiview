package com.appersonic.multiview.config

import com.appersonic.multiview.constants.ADD_ICE_CANDIDATE_DESTINATION_PREFIX
import com.appersonic.multiview.constants.APPLICATION_DESTINATION_PREFIX
import com.appersonic.multiview.constants.CLEAR_MAGNIFICATION_PREFIX
import com.appersonic.multiview.constants.CONNECT_DESTINATION_PREFIX
import com.appersonic.multiview.constants.DISCONNECT_DESTINATION_PREFIX
import com.appersonic.multiview.constants.ERRORS_DESTINATION_PREFIX
import com.appersonic.multiview.constants.MAGNIFY_PREFIX
import com.appersonic.multiview.constants.PARTICIPANT_DESTINATION_PREFIX
import com.appersonic.multiview.constants.SET_PORT_METADATA_PREFIX
import com.appersonic.multiview.constants.STOMP_MESSAGING_ENDPOINT
import com.appersonic.multiview.constants.UPDATE_FLEX_GRID_LAYOUT_PREFIX
import org.springframework.context.annotation.Configuration
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer

@Configuration
@EnableWebSocketMessageBroker
class WebSocketConfig: WebSocketMessageBrokerConfigurer {
    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
        registry.addEndpoint(STOMP_MESSAGING_ENDPOINT)
            .withSockJS()
    }

    override fun configureMessageBroker(registry: MessageBrokerRegistry) {
        registry.enableSimpleBroker(
            CONNECT_DESTINATION_PREFIX,
            DISCONNECT_DESTINATION_PREFIX,
            ADD_ICE_CANDIDATE_DESTINATION_PREFIX,
            ERRORS_DESTINATION_PREFIX,
            PARTICIPANT_DESTINATION_PREFIX,
            UPDATE_FLEX_GRID_LAYOUT_PREFIX,
            MAGNIFY_PREFIX,
            CLEAR_MAGNIFICATION_PREFIX,
            SET_PORT_METADATA_PREFIX
        )
        registry.setApplicationDestinationPrefixes(
            APPLICATION_DESTINATION_PREFIX
        )
    }
}

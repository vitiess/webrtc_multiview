package com.appersonic.multiview.message

data class SetPortMetadataMessage(
    val port: String,
    val friendlyName: String
)

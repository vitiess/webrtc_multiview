package com.appersonic.multiview.message

import com.appersonic.multiview.model.SdpAnswer
import com.appersonic.multiview.model.SessionParticipant

data class ConnectReply(
    val sdpAnswer: SdpAnswer,
    val participants: List<SessionParticipant>
)

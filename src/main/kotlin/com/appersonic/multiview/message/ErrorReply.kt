package com.appersonic.multiview.message

data class ErrorReply(val errorMessage: String) {
    init {
        require(errorMessage.isNotBlank()) { "errorMessage must not be blank." }
    }
}

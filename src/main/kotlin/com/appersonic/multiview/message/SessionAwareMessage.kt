package com.appersonic.multiview.message

import com.appersonic.multiview.model.SessionId

data class SessionAwareMessage<M>(val payload: M, val sessionId: SessionId);

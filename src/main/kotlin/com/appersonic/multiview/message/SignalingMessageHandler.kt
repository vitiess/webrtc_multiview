package com.appersonic.multiview.message

interface SignalingMessageHandler {
    fun handleConnectMessage(message: SessionAwareMessage<ConnectMessage>): ConnectReply
    fun handleDisconnectMessage(message: SessionAwareMessage<DisconnectMessage>): DisconnectReply
    fun handleIceCandidateMessage(message: SessionAwareMessage<AddIceCandidateMessage>): AddIceCandidateReply
    fun handleFlexGridLayoutUpdate(message: SessionAwareMessage<UpdateFlexGridLayoutMessage>): UpdateFlexGridLayoutReply
    fun handleMagnificationMessage(message: SessionAwareMessage<MagnifyMessage>): MagnifyMessageReply
    fun handleClearMagnificationMessage(message: SessionAwareMessage<ClearMagnificationMessage>): ClearMagnificationReply
    fun handleSetPortMetadataMessage(message: SessionAwareMessage<SetPortMetadataMessage>): SetPortMetadataReply
}

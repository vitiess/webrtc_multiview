package com.appersonic.multiview.message

import com.appersonic.multiview.webrtc.SetPortMetadataParams
import com.appersonic.multiview.webrtc.WebRtcAddIceCandidateParams
import com.appersonic.multiview.webrtc.WebRtcClearMagnificationParams
import com.appersonic.multiview.webrtc.WebRtcGetAllParticipantsParams
import com.appersonic.multiview.webrtc.WebRtcJoinSessionParams
import com.appersonic.multiview.webrtc.WebRtcLeaveSessionParams
import com.appersonic.multiview.webrtc.WebRtcMagnifyParams
import com.appersonic.multiview.webrtc.WebRtcSessionFactory
import com.appersonic.multiview.webrtc.WebRtcUpdateGridParams

class SingleSessionSignalingHandler(
    webRtcSessionFactory: WebRtcSessionFactory
): SignalingMessageHandler
{
    private val webRtcSession = webRtcSessionFactory.create()

    override fun handleConnectMessage(message: SessionAwareMessage<ConnectMessage>): ConnectReply {
        val joinSessionParams = WebRtcJoinSessionParams(
            actor = message.payload.actor!!,
            sdpOffer = message.payload.sdpOffer!!,
            role = message.payload.role!!,
            sessionId = message.sessionId
        )
        val joinSessionResult = webRtcSession.join(joinSessionParams)
        return ConnectReply(
            joinSessionResult.sdpAnswer,
            webRtcSession.getAllParticipants(WebRtcGetAllParticipantsParams()).participants
        )
    }

    override fun handleDisconnectMessage(message: SessionAwareMessage<DisconnectMessage>): DisconnectReply {
        val leaveSessionParams = WebRtcLeaveSessionParams(
            sessionId = message.sessionId
        )
        webRtcSession.leave(leaveSessionParams)
        return DisconnectReply()
    }

    override fun handleIceCandidateMessage(message: SessionAwareMessage<AddIceCandidateMessage>): AddIceCandidateReply {
        val addIceCandidateParams = WebRtcAddIceCandidateParams(
            iceCandidate = message.payload.candidate!!,
            sessionId = message.sessionId
        )
        webRtcSession.addIceCandidate(addIceCandidateParams)
        return AddIceCandidateReply()
    }

    override fun handleFlexGridLayoutUpdate(message: SessionAwareMessage<UpdateFlexGridLayoutMessage>): UpdateFlexGridLayoutReply {
        val updateGridParams = WebRtcUpdateGridParams(
            sessionId = message.sessionId,
            width = message.payload.width,
            height = message.payload.height,
            horizontalSpacing = message.payload.horizontalSpacing,
            verticalSpacing = message.payload.verticalSpacing,
            topPadding = message.payload.topPadding,
            rightPadding = message.payload.rightPadding,
            bottomPadding = message.payload.bottomPadding,
            leftPadding = message.payload.leftPadding,
            maxRows = message.payload.maxRows,
            maxCols = message.payload.maxCols,
            alignmentPolicy = message.payload.alignmentPolicy
        )
        webRtcSession.updateGrid(updateGridParams)
        return UpdateFlexGridLayoutReply()
    }

    override fun handleMagnificationMessage(message: SessionAwareMessage<MagnifyMessage>): MagnifyMessageReply {
        val magnifyParams = WebRtcMagnifyParams(
            port = message.payload.port
        )
        webRtcSession.magnify(magnifyParams)
        return MagnifyMessageReply()
    }

    override fun handleClearMagnificationMessage(message: SessionAwareMessage<ClearMagnificationMessage>): ClearMagnificationReply {
        webRtcSession.clearMagnification(WebRtcClearMagnificationParams())
        return ClearMagnificationReply()
    }

    override fun handleSetPortMetadataMessage(message: SessionAwareMessage<SetPortMetadataMessage>): SetPortMetadataReply {
        webRtcSession.setPortMetadata(SetPortMetadataParams(message.payload.port, message.payload.friendlyName))
        return SetPortMetadataReply()
    }
}

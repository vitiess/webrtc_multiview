package com.appersonic.multiview.message

import com.appersonic.multiview.model.SessionParticipant

class SessionParticipantsUpdatedMessage(
    val added: List<SessionParticipant> = emptyList(),
    val removed: List<SessionParticipant> = emptyList()
)

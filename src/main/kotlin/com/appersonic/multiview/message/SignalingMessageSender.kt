package com.appersonic.multiview.message

import com.appersonic.multiview.model.SessionId
import com.appersonic.multiview.model.SessionParticipant

interface SignalingMessageSender {
    fun sendIceCandidate(candidate: String, sessionId: SessionId)
    fun sendParticipantAdded(participant: SessionParticipant)
    fun sendParticipantRemoved(participant: SessionParticipant)
}

package com.appersonic.multiview.message

import com.appersonic.multiview.constants.ADD_ICE_CANDIDATE_DESTINATION_PREFIX
import com.appersonic.multiview.constants.PARTICIPANT_DESTINATION_PREFIX
import com.appersonic.multiview.model.SessionId
import com.appersonic.multiview.model.SessionParticipant
import org.springframework.messaging.simp.SimpMessageHeaderAccessor
import org.springframework.messaging.simp.SimpMessageType
import org.springframework.messaging.simp.SimpMessagingTemplate

class StompSignalingMessageSender(private val messagingTemplate: SimpMessagingTemplate): SignalingMessageSender {
    override fun sendIceCandidate(candidate: String, sessionId: SessionId) {
        val headerAccessor = createMessageHeaderAccessor(sessionId.id!!)
        messagingTemplate.convertAndSendToUser(
            sessionId.id,
            ADD_ICE_CANDIDATE_DESTINATION_PREFIX,
            candidate,
            headerAccessor.messageHeaders
        )
    }

    private fun createMessageHeaderAccessor(sessionId: String): SimpMessageHeaderAccessor {
        val accessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE)
        accessor.sessionId = sessionId
        accessor.setLeaveMutable(true)
        return accessor
    }

    override fun sendParticipantAdded(participant: SessionParticipant) {
        messagingTemplate.convertAndSend(
            PARTICIPANT_DESTINATION_PREFIX,
            SessionParticipantsUpdatedMessage(
                added = listOf(participant)
            )
        )
    }

    override fun sendParticipantRemoved(participant: SessionParticipant) {
        messagingTemplate.convertAndSend(
            PARTICIPANT_DESTINATION_PREFIX,
            SessionParticipantsUpdatedMessage(
                removed = listOf(participant)
            )
        )
    }
}

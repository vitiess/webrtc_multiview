package com.appersonic.multiview.message

data class UpdateFlexGridLayoutMessage(
    val width: Int,
    val height: Int,
    val horizontalSpacing: Int,
    val verticalSpacing: Int,
    val topPadding: Int,
    val rightPadding: Int,
    val bottomPadding: Int,
    val leftPadding: Int,
    val maxRows: Int,
    val maxCols: Int,
    val alignmentPolicy: Int
)
{
    init {
        require(width > 0) { "width must be greater than 0" }
        require(height > 0) { "height must be greater than 0" }
        require(horizontalSpacing >= 0) { "horizontalSpacing must be greater or equal to 0" }
        require(verticalSpacing >= 0) { "verticalSpacing must be greater or equal to 0" }
        require(topPadding >= 0) { "topPadding must be greater or equal to 0" }
        require(rightPadding >= 0) { "rightPadding must be greater or equal to 0" }
        require(bottomPadding >= 0) { "bottomPadding must be greater or equal to 0" }
        require(leftPadding >= 0) { "leftPadding must be greater or equal to 0" }
        require(maxRows >= 0) { "maxRows must be greater or equal to 0" }
        require(maxCols >= 0) { "maxCols must be greater or equal to 0" }
        require(alignmentPolicy >= 0) { "alignmentPolicy must be greater or equal to 0" }
    }
}

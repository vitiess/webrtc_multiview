package com.appersonic.multiview.message

import com.appersonic.multiview.model.Actor
import com.appersonic.multiview.model.SdpOffer
import com.appersonic.multiview.model.SessionRole

data class ConnectMessage(
    val actor: Actor?,
    val sdpOffer: SdpOffer?,
    val role: SessionRole?
)
{
    init {
        requireNotNull(actor) { "actor must not be null." }
        requireNotNull(sdpOffer) { "sdpOffer must not be null." }
        requireNotNull(role) { "role must not be null." }
    }
}

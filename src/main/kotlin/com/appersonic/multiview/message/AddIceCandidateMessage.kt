package com.appersonic.multiview.message

import com.appersonic.multiview.model.IceCandidate

class AddIceCandidateMessage(val candidate: IceCandidate?) {
    init {
        requireNotNull(candidate) {
            "Interactive Connectivity Establishment (ICE) candidate must not be null."
        }
    }
}

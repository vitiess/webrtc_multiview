package com.appersonic.multiview.webrtc

import com.appersonic.multiview.model.IceCandidate
import com.appersonic.multiview.model.SessionId

data class WebRtcAddIceCandidateParams(
    val iceCandidate: IceCandidate,
    val sessionId: SessionId
)

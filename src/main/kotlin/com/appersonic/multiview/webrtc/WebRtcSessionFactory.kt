package com.appersonic.multiview.webrtc

interface WebRtcSessionFactory {
    fun create(): WebRtcSession
}

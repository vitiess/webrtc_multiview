package com.appersonic.multiview.webrtc

import com.appersonic.multiview.model.SessionId

data class WebRtcLeaveSessionParams(val sessionId: SessionId)

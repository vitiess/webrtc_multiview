package com.appersonic.multiview.webrtc

import com.appersonic.multiview.model.SdpAnswer

data class WebRtcJoinSessionResult(val sdpAnswer: SdpAnswer)

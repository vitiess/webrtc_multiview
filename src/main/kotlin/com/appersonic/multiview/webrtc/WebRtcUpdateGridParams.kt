package com.appersonic.multiview.webrtc

import com.appersonic.multiview.model.SessionId

class WebRtcUpdateGridParams(
    val sessionId: SessionId,
    val width: Int,
    val height: Int,
    val horizontalSpacing: Int,
    val verticalSpacing: Int,
    val topPadding: Int,
    val rightPadding: Int,
    val bottomPadding: Int,
    val leftPadding: Int,
    val maxRows: Int,
    val maxCols: Int,
    val alignmentPolicy: Int
)

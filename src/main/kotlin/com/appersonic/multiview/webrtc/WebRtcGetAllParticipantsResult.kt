package com.appersonic.multiview.webrtc

import com.appersonic.multiview.model.SessionParticipant

class WebRtcGetAllParticipantsResult(val participants: List<SessionParticipant>);

package com.appersonic.multiview.webrtc

interface WebRtcSession {
    fun join(params: WebRtcJoinSessionParams): WebRtcJoinSessionResult
    fun leave(params: WebRtcLeaveSessionParams): WebRtcLeaveSessionResult
    fun addIceCandidate(params: WebRtcAddIceCandidateParams): WebRtcAddIceCandidateResult
    fun getAllParticipants(params: WebRtcGetAllParticipantsParams): WebRtcGetAllParticipantsResult
    fun updateGrid(params: WebRtcUpdateGridParams): WebRtcUpdateGridResult
    fun magnify(params: WebRtcMagnifyParams): WebRtcMagnifyResult
    fun clearMagnification(params: WebRtcClearMagnificationParams): WebRtcClearMagnificationResult
    fun setPortMetadata(param: SetPortMetadataParams): SetPortMetadataResult
}

package com.appersonic.multiview.webrtc

import com.appersonic.multiview.model.SdpOffer
import com.appersonic.multiview.model.SessionId
import com.appersonic.multiview.model.SessionRole

interface WebRtcSessionActor {
    val name: String
    val sessionId: SessionId
    val role: SessionRole
    val sdpOffer: SdpOffer
}

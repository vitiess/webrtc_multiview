package com.appersonic.multiview.webrtc

class SetPortMetadataParams(
    val portId: String,
    val friendlyName: String
)

package com.appersonic.multiview.webrtc

import com.appersonic.multiview.model.Actor
import com.appersonic.multiview.model.SdpOffer
import com.appersonic.multiview.model.SessionId
import com.appersonic.multiview.model.SessionRole

data class WebRtcJoinSessionParams(
    val actor: Actor,
    val sdpOffer: SdpOffer,
    val role: SessionRole,
    val sessionId: SessionId
)

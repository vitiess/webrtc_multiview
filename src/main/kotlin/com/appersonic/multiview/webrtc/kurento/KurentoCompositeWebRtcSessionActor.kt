package com.appersonic.multiview.webrtc.kurento

import com.appersonic.multiview.model.SdpOffer
import com.appersonic.multiview.model.SessionId
import com.appersonic.multiview.model.SessionRole
import com.appersonic.multiview.webrtc.WebRtcSessionActor
import org.kurento.client.MediaElement
import org.kurento.client.WebRtcEndpoint

class KurentoCompositeWebRtcSessionActor(
    override val name: String,
    override val sessionId: SessionId,
    override val role: SessionRole,
    override val sdpOffer: SdpOffer,
    val endpoint: WebRtcEndpoint,
    val hubPort: MediaElement
): WebRtcSessionActor

package com.appersonic.multiview.webrtc.kurento

import org.kurento.client.EventListener
import org.kurento.client.IceComponentStateChangeEvent
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class KurentoIceComponentStateChangeListener(
    private val actor: KurentoCompositeWebRtcSessionActor
): EventListener<IceComponentStateChangeEvent>
{
    private val log: Logger = LoggerFactory.getLogger(KurentoIceCandidateFoundEventListener::class.java)

    override fun onEvent(event: IceComponentStateChangeEvent) {
        log.info("ICE connectivity state is {} for session {}", event.state, actor.sessionId.id);
    }
}

package com.appersonic.multiview.webrtc.kurento

import com.appersonic.multiview.message.SignalingMessageSender
import org.kurento.client.EventListener
import org.kurento.client.IceCandidateFoundEvent
import org.kurento.jsonrpc.JsonUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class KurentoIceCandidateFoundEventListener(
    private val actor: KurentoCompositeWebRtcSessionActor,
    private val signalingMessageSender: SignalingMessageSender
): EventListener<IceCandidateFoundEvent>
{
    private val log: Logger = LoggerFactory.getLogger(KurentoIceCandidateFoundEventListener::class.java)

    override fun onEvent(event: IceCandidateFoundEvent) {
        val candidate = JsonUtils.toJsonObject(event.candidate)
        log.info("[{}/{}] received ICE candidate from KMS (={})", actor.name, actor.sessionId.id, candidate.toString())
        signalingMessageSender.sendIceCandidate(candidate.toString(), actor.sessionId)
    }
}

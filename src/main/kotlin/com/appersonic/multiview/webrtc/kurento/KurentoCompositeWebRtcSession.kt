package com.appersonic.multiview.webrtc.kurento

import com.appersonic.multiview.message.SignalingMessageSender
import com.appersonic.multiview.model.IceCandidate
import com.appersonic.multiview.model.SdpAnswer
import com.appersonic.multiview.model.SessionId
import com.appersonic.multiview.model.SessionParticipant
import com.appersonic.multiview.model.SessionRole
import com.appersonic.multiview.model.SignalingServerException
import com.appersonic.multiview.webrtc.SetPortMetadataParams
import com.appersonic.multiview.webrtc.SetPortMetadataResult
import com.appersonic.multiview.webrtc.WebRtcAddIceCandidateParams
import com.appersonic.multiview.webrtc.WebRtcAddIceCandidateResult
import com.appersonic.multiview.webrtc.WebRtcClearMagnificationParams
import com.appersonic.multiview.webrtc.WebRtcClearMagnificationResult
import com.appersonic.multiview.webrtc.WebRtcGetAllParticipantsParams
import com.appersonic.multiview.webrtc.WebRtcGetAllParticipantsResult
import com.appersonic.multiview.webrtc.WebRtcJoinSessionParams
import com.appersonic.multiview.webrtc.WebRtcJoinSessionResult
import com.appersonic.multiview.webrtc.WebRtcLeaveSessionParams
import com.appersonic.multiview.webrtc.WebRtcLeaveSessionResult
import com.appersonic.multiview.webrtc.WebRtcMagnifyParams
import com.appersonic.multiview.webrtc.WebRtcMagnifyResult
import com.appersonic.multiview.webrtc.WebRtcSession
import com.appersonic.multiview.webrtc.WebRtcUpdateGridParams
import com.appersonic.multiview.webrtc.WebRtcUpdateGridResult
import org.kurento.client.HubPort
import org.kurento.client.KurentoClient
import org.kurento.client.WebRtcEndpoint
import org.kurento.module.flexcomposite.FlexComposite
import org.kurento.module.flexcomposite.FlexGridClearMagnificationParams
import org.kurento.module.flexcomposite.FlexGridLayout
import org.kurento.module.flexcomposite.FlexGridMagnificationParams
import org.kurento.module.flexcomposite.FlexGridPadding
import org.kurento.module.flexcomposite.FlexGridPortMetadata
import org.kurento.module.flexcomposite.FlexGridSetPortMetadataParams
import org.kurento.module.flexcomposite.FlexGridSize
import org.kurento.module.flexcomposite.FlexGridSpacing
import org.kurento.module.flexcomposite.FlexGridSpec
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.ConcurrentHashMap

class KurentoCompositeWebRtcSession(
    kurentoClient: KurentoClient,
    private val signalingMessageSender: SignalingMessageSender
): WebRtcSession
{
    private val log: Logger = LoggerFactory.getLogger(KurentoCompositeWebRtcSession::class.java)
    private val mediaPipeline = kurentoClient.createMediaPipeline()
    private val actorsBySessionId = ConcurrentHashMap<SessionId, KurentoCompositeWebRtcSessionActor>()
    private val iceCandidatesBySessionId = ConcurrentHashMap<SessionId, MutableList<IceCandidate>>()
    private val composite = FlexComposite.Builder(mediaPipeline, DEFAULT_GRID_SPEC).build()

    override fun join(params: WebRtcJoinSessionParams): WebRtcJoinSessionResult {
        validate(params)
        val webRtcEndpoint = WebRtcEndpoint.Builder(mediaPipeline).build()
        val sdpAnswer = webRtcEndpoint.processOffer(params.sdpOffer.offer)
            ?: throw SignalingServerException.createWithMessage("Could not accept SDP offer.")
        val hubPort = HubPort.Builder(composite).build()

        val actor = createActor(params, webRtcEndpoint, hubPort)
        if (actor.role == SessionRole.VIEWER) {
            joinAsViewer(actor)
        } else {
            joinAsPresenter(actor)
        }
        webRtcEndpoint.addIceCandidateFoundListener(
            KurentoIceCandidateFoundEventListener(actor, signalingMessageSender)
        )
        webRtcEndpoint.addIceComponentStateChangeListener(
            KurentoIceComponentStateChangeListener(actor)
        )
        webRtcEndpoint.gatherCandidates()
        actorsBySessionId[actor.sessionId] = actor
        log.info("Registered session {}", actor.sessionId.id)
        processIceCandidatesForActor(actor)
        onParticipantAdded(actor)
        return WebRtcJoinSessionResult(SdpAnswer(sdpAnswer))
    }

    private fun validate(joinParams: WebRtcJoinSessionParams) {
        if (actorsBySessionId.contains(joinParams.sessionId)) {
            throw SignalingServerException("${joinParams.actor.name}/${joinParams.sessionId} already joined.")
        }
    }

    private fun createActor(
        joinParams: WebRtcJoinSessionParams,
        endpoint: WebRtcEndpoint,
        hubPort: HubPort
    ): KurentoCompositeWebRtcSessionActor {
        return KurentoCompositeWebRtcSessionActor(
            joinParams.actor.name!!,
            joinParams.sessionId,
            joinParams.role,
            joinParams.sdpOffer,
            endpoint,
            hubPort
        )
    }

    private fun joinAsViewer(actor: KurentoCompositeWebRtcSessionActor) {
        actor.hubPort.connect(actor.endpoint)
    }

    private fun joinAsPresenter(actor: KurentoCompositeWebRtcSessionActor) {
        actor.endpoint.connect(actor.hubPort)
    }

    override fun leave(params: WebRtcLeaveSessionParams): WebRtcLeaveSessionResult {
        val actor = getActorOrThrow(params.sessionId)
        actor.hubPort.release()
        actor.endpoint.release()
        actorsBySessionId.remove(params.sessionId)
        onParticipantRemoved(actor)
        return WebRtcLeaveSessionResult()
    }

    private fun getActorOrThrow(actorSessionId: SessionId): KurentoCompositeWebRtcSessionActor {
        return actorsBySessionId[actorSessionId]
            ?: throw SignalingServerException.createWithMessage("Unknown session $actorSessionId")
    }

    private fun onParticipantAdded(actor: KurentoCompositeWebRtcSessionActor) {
        signalingMessageSender.sendParticipantAdded(SessionParticipant(actor.name, actor.role, actor.sessionId.id!!))
    }

    override fun addIceCandidate(params: WebRtcAddIceCandidateParams): WebRtcAddIceCandidateResult {
        val actor = getActorNoThrow(params.sessionId)
        if (actor != null) {
            log.info("Receiving ICE candidate for session {}", params.sessionId.id)
            actor.endpoint.addIceCandidate(toKurentoIceCandidate(params.iceCandidate))
            processIceCandidatesForActor(actor)
        } else {
            log.warn("Session {} is not ready yet. Saving ICE candidate for later.", params.sessionId.id)
            iceCandidatesBySessionId.compute(params.sessionId) { _, v -> (v ?: mutableListOf()).apply { add(params.iceCandidate) } }
        }
        return WebRtcAddIceCandidateResult()
    }

    private fun processIceCandidatesForActor(actor: KurentoCompositeWebRtcSessionActor) {
        val candidates = iceCandidatesBySessionId[actor.sessionId] ?: mutableListOf()
        for (candidate in candidates) {
            log.info("Sending ICE candidate to Kurento for session {}", actor.sessionId)
            actor.endpoint.addIceCandidate(toKurentoIceCandidate(candidate))
        }
        iceCandidatesBySessionId.remove(actor.sessionId)
    }

    private fun getActorNoThrow(actorSessionId: SessionId): KurentoCompositeWebRtcSessionActor? {
        return actorsBySessionId[actorSessionId]
    }

    private fun toKurentoIceCandidate(iceCandidate: IceCandidate): org.kurento.client.IceCandidate {
        return org.kurento.client.IceCandidate(
            iceCandidate.candidate!!,
            iceCandidate.sdpMid!!,
            iceCandidate.sdpMLineIndex!!
        )
    }

    override fun getAllParticipants(params: WebRtcGetAllParticipantsParams): WebRtcGetAllParticipantsResult {
        return WebRtcGetAllParticipantsResult(
            participants = actorsBySessionId.values.map {
                SessionParticipant(it.name, it.role, it.sessionId.id!!)
            }.toList()
        )
    }

    private fun onParticipantRemoved(actor: KurentoCompositeWebRtcSessionActor) {
        signalingMessageSender.sendParticipantRemoved(SessionParticipant(actor.name, actor.role, actor.sessionId.id!!))
    }

    override fun updateGrid(params: WebRtcUpdateGridParams): WebRtcUpdateGridResult {
        val spec = toFlexGridSpec(params)
        log.info("Flex grid layout update. Width=${spec.size.width}, Height=${spec.size.height}, " +
                "HorizontalSpacing=${spec.spacing.horizontal}, VerticalSpacing=${spec.spacing.vertical}, " +
                "TopPadding=${spec.padding.top}, RightPadding=${spec.padding.right}, BottomPadding=${spec.padding.bottom}" +
                "LeftPadding=${spec.padding.left}, MaxRows=${spec.layout.numRows}, MaxColumns=${spec.layout.numCols}")
        composite.updateGridSpec(spec)
        return WebRtcUpdateGridResult()
    }

    private fun toFlexGridSpec(params: WebRtcUpdateGridParams): FlexGridSpec {
        val size = FlexGridSize(params.width, params.height)
        val spacing = FlexGridSpacing(params.horizontalSpacing, params.verticalSpacing)
        val padding = FlexGridPadding(params.topPadding, params.rightPadding, params.bottomPadding, params.leftPadding)
        val layout = FlexGridLayout(params.maxRows, params.maxCols, 3, params.alignmentPolicy)
        return FlexGridSpec(size, padding, spacing, layout)
    }

    override fun magnify(params: WebRtcMagnifyParams): WebRtcMagnifyResult {
        val magnifyParams = FlexGridMagnificationParams(params.port)
        composite.magnify(magnifyParams)
        return WebRtcMagnifyResult()
    }

    override fun clearMagnification(params: WebRtcClearMagnificationParams): WebRtcClearMagnificationResult {
        composite.clearMagnification(FlexGridClearMagnificationParams())
        return WebRtcClearMagnificationResult()
    }

    override fun setPortMetadata(param: SetPortMetadataParams): SetPortMetadataResult {
        val metadata = FlexGridPortMetadata(param.friendlyName)
        val args = FlexGridSetPortMetadataParams(param.portId, metadata)
        composite.setPortMetadata(args)
        return SetPortMetadataResult()
    }

    private companion object {
        val DEFAULT_GRID_SPEC = FlexGridSpec(
            FlexGridSize(800, 600),
            FlexGridPadding(0, 0, 0, 0),
            FlexGridSpacing(10, 10),
            FlexGridLayout(2, 2, 3, 0)
        );
    }
}

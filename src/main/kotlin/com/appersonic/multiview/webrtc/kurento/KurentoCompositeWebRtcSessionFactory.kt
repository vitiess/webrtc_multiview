package com.appersonic.multiview.webrtc.kurento

import com.appersonic.multiview.message.SignalingMessageSender
import com.appersonic.multiview.webrtc.WebRtcSession
import com.appersonic.multiview.webrtc.WebRtcSessionFactory
import org.kurento.client.KurentoClient

class KurentoCompositeWebRtcSessionFactory(
    kurentoClient: KurentoClient,
    signalingMessageSender: SignalingMessageSender
): WebRtcSessionFactory
{
    private val singletonSession = KurentoCompositeWebRtcSession(kurentoClient, signalingMessageSender)

    override fun create(): WebRtcSession {
        return singletonSession
    }
}

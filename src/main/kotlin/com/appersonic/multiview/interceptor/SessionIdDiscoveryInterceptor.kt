package com.appersonic.multiview.interceptor

import com.appersonic.multiview.constants.SESSION_ID_ATTRIBUTE
import org.springframework.http.server.ServerHttpRequest
import org.springframework.http.server.ServerHttpResponse
import org.springframework.http.server.ServletServerHttpRequest
import org.springframework.web.socket.WebSocketHandler
import org.springframework.web.socket.server.HandshakeInterceptor

class SessionIdDiscoveryInterceptor: HandshakeInterceptor {
    override fun beforeHandshake(
        request: ServerHttpRequest,
        response: ServerHttpResponse,
        wsHandler: WebSocketHandler,
        attributes: MutableMap<String, Any>
    ): Boolean
    {
        if (request is ServletServerHttpRequest) {
            request.servletRequest.session?.let { attributes.put(SESSION_ID_ATTRIBUTE, it.id) }
        }
        return true
    }

    override fun afterHandshake(
        request: ServerHttpRequest,
        response: ServerHttpResponse,
        wsHandler: WebSocketHandler,
        exception: Exception?
    )
    {
        // Nothing required here.
    }
}
